﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyHobbies
{
    public class Band
    {
        public Band(string naam, int oprichtjaar)
        {
            this.Naam = naam;
            this.Oprichtjaar = oprichtjaar;
            this.Bandleden = new List<Bandlid>();
        }

        public string Naam { get; set; }

        public int Oprichtjaar { get; set; }

        public List<Bandlid> Bandleden { get; set; }
    }
}
