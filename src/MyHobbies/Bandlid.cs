﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyHobbies
{
    public enum Geslacht
    {
        MAN,
        VROUW
    }

    public class Bandlid
    {
        public Bandlid(string naam, int leeftijd, Geslacht geslacht, bool gestorven)
        {
            this.Naam = naam;
            this.Leeftijd = leeftijd;
            this.Geslacht = geslacht;
            this.Gestorven = gestorven;
        }

        public string Naam { get; set; }

        public int Leeftijd { get; set; }

        public Geslacht Geslacht { get; set; }

        public bool Gestorven { get; set; }
    }
}
