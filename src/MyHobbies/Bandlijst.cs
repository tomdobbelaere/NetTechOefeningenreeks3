﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyHobbies
{
    public class Bandlijst
    {
        private static Bandlijst instance;

        public Bandlijst()
        {
            this.Bands = new List<Band>();

            //Add bands here
            Band newBand = new Band("Sample Band", 2014);
            newBand.Bandleden.Add(new Bandlid("Some guy", 18, Geslacht.MAN, false));

            this.Bands.Add(newBand);

            newBand = new Band("Other Band", 2000);
            newBand.Bandleden.Add(new Bandlid("Some girl", 23, Geslacht.VROUW, false));

            this.Bands.Add(newBand);
        }

        public static Bandlijst Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Bandlijst();
                }

                return instance;
            }
        }

        public List<Band> Bands { get; set; }
    }
}
