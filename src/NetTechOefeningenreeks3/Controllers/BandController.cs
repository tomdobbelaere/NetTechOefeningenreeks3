﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyHobbies;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace NetTechOefeningenreeks3.Controllers
{
    public class BandController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Lijst()
        {
            ViewBag.lijst = Bandlijst.Instance.Bands;
            ViewBag.toonLeden = false;

            return View();
        }

        public IActionResult LijstMetLeden()
        {
            ViewBag.lijst = Bandlijst.Instance.Bands;
            ViewBag.toonLeden = true;

            return View("Lijst");
        }

        public IActionResult Maak(string bandNaam, int jaar)
        {
            Band nieuweBand = new Band(bandNaam, jaar);

            ViewBag.nieuweBand = nieuweBand;

            Bandlijst.Instance.Bands.Add(nieuweBand);

            return View();
        }
    }
}
